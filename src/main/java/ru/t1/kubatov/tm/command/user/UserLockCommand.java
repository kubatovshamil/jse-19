package ru.t1.kubatov.tm.command.user;

import ru.t1.kubatov.tm.enumerated.Role;
import ru.t1.kubatov.tm.util.TerminalUtil;

public class UserLockCommand extends AbstractUserCommand {

    public final static String DESCRIPTION = "Lock User.";

    public final static String NAME = "user-lock";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[LOCK USER]");
        System.out.println("Enter login:");
        final String login = TerminalUtil.nextLine();
        getUserService().lockUserByLogin(login);
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }
}
