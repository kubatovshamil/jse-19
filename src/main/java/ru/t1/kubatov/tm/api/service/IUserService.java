package ru.t1.kubatov.tm.api.service;

import ru.t1.kubatov.tm.api.repository.IUserRepository;
import ru.t1.kubatov.tm.enumerated.Role;
import ru.t1.kubatov.tm.model.User;

public interface IUserService extends IUserRepository {

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User deleteByLogin(String login);

    User deleteByEmail(String email);

    User setPassword(String id, String password);

    User updateUser(String id, String firstName, String lastName, String middleName);

    void lockUserByLogin(String login);

    void unlockUserByLogin(String login);

}
