package ru.t1.kubatov.tm.api.service;

import ru.t1.kubatov.tm.model.Task;

public interface IProjectTaskService {

    Task bindTaskToProject(String userId, String projectId, String taskId);

    void removeProjectById(String userId, String projectId);

    void removeProjects(String userId);

    Task unbindTaskFromProject(String userId, String projectId, String taskId);

}
